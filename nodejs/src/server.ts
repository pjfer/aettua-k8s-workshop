import { createServer, IncomingMessage, ServerResponse } from 'http'
 
const port = 8080
 
const server = createServer((request: IncomingMessage, response: ServerResponse) => {
    response.writeHead(200)
    response.end('Hello world!');
})

server.listen(port, () => console.log(`Server listening on port ${port}`))
